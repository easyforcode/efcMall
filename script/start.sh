#!/bin/bash

# 查找并终止运行中的 Java 服务
SERVICE_NAME="mall.jar"

echo "查找并终止运行中的 Java 服务..."
JAVA_PROCESS=$(ps -ef | grep "$SERVICE_NAME" | grep -v grep)
if [ -n "$JAVA_PROCESS" ]; then
    PID=$(echo "$JAVA_PROCESS" | awk '{print $2}')
    echo "Found Java process running with PID $PID. Terminating..."
    kill $PID
else
    echo "No Java process found running."
fi

# 启动 Java 程序
echo "Starting Java program..."
nohup /home/soft/jdk1.8/bin/java -jar /home/project/$SERVICE_NAME --server.port=8080 > /home/project/english/catalina.out 2>&1 &

#!/bin/bash

# 设置轮询次数
MAX_TRIES=100
# 设置轮询间隔（秒）
INTERVAL=2
# 设置服务监听地址和端口

tries=0
while [ $tries -lt $MAX_TRIES ]; do
    if ps aux | grep java | grep "$SERVICE_NAME" | grep -v grep > /dev/null; then
        echo "Service is up and running!"
        exit 0
    fi
    tries=$((tries + 1))
    sleep $INTERVAL
done

echo "Service did not start within the specified time."
exit 1



