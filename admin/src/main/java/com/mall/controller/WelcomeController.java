package com.mall.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class WelcomeController {
    private Log logger = LogFactory.getLog(IndexController.class);
    @RequestMapping("")
    public String welcome() {
        return "index";
    }

}
