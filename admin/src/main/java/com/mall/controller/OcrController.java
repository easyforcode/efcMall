package com.mall.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;


@Controller
@RequestMapping("/ocr")
public class OcrController {
    private Log logger = LogFactory.getLog(OcrController.class);
    private String secretId = "";
    private String secretKey = "";

    @ResponseBody
    @RequestMapping(value = "/uploadImage")
    public List<String> uploadImage(@RequestBody MultipartFile file)  throws IOException {
        if (file != null && !file.isEmpty()) {
            // 读取文件内容并转换为 Base64 编码的字符串
            byte[] bytes = file.getBytes();
            String base64Image = Base64.getEncoder().encodeToString(bytes);
        }
        return new ArrayList<>();
    }


    public static String convertImageToBase64(String imagePath) {
        File file = new File(imagePath);
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            byte[] buffer = new byte[(int) file.length()];
            fileInputStream.read(buffer);

            // 使用Base64编码器将字节数组转换为Base64字符串
            return Base64.getEncoder().encodeToString(buffer);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
