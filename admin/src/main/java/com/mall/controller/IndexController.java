package com.mall.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/admin")
public class IndexController {
    private Log logger = LogFactory.getLog(IndexController.class);

    @RequestMapping("/home")
    public String home(HttpServletRequest request, HttpServletResponse response) {
        return "admin/home";
    }



}
