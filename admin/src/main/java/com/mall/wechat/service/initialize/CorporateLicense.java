package com.mall.wechat.service.initialize;


import com.mall.wechat.bean.CorporateLicense.Admin_list;
import com.mall.wechat.bean.CorporateLicense.CorporateBean;
import com.mall.wechat.bean.TokenBean;
import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.JSONBody;

public interface CorporateLicense {

    /**
     * https://work.weixin.qq.com/api/doc/90001/90143/90601
     * 获取预授权码
     * 该API用于获取预授权码。预授权码用于企业授权时的第三方服务商安全验证。
     * <p>
     * 请求方式：GET（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code?suite_access_token=SUITE_ACCESS_TOKEN
     *
     * @param suite_access_token 服务商的access_token,最长为512字节
     * @return {
     * "errcode":0 ,
     * "errmsg":"ok" ,
     * "pre_auth_code":"Cx_Dk6qiBE0Dmx4EmlT3oRfArPvwSQ-oa3NL_fwHM7VI08r52wazoZX2Rhpz1dEw",
     * "expires_in":1200
     * }
     */
    @Get("https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code")
    public TokenBean get_pre_auth_code(@Body("suite_access_token") String suite_access_token);


    /**
     * https://work.weixin.qq.com/api/doc/90001/90143/90597
     * 引导用户进入授权页
     * 第三方服务商在自己的网站中放置“企业微信应用授权”的入口，引导企业微信管理员进入应用授权页。授权页网址为:
     * <p>
     * 请求方式：GET（HTTPS）
     * 请求地址： https://open.work.weixin.qq.com/3rdapp/install?suite_id=SUITE_ID&pre_auth_code=PRE_AUTH_CODE&redirect_uri=REDIRECT_URI&state=STATE
     *
     * @param suite_id      第三方服务商需提供suite_id 个人觉得是corpID
     * @param pre_auth_code 预授权码 get_pre_auth_code 接口获取
     * @param redirect_uri  授权完成回调URI,redirect_uri是授权完成后的回调网址，redirect_uri需要经过一次urlencode作为参数
     * @param state         state参数 可填a-zA-Z0-9的参数值（不超过128个字节），用于第三方自行校验session，防止跨域攻击
     * @return redirect_uri?auth_code=xxx&expires_in=600&state=xx
     * 临时授权码auth_code10分钟后会失效，第三方服务商需尽快使用临时授权码换取永久授权码及授权信息。
     * 每个企业授权的每个应用的永久授权码、授权信息都是唯一的，第三方服务商需妥善保管。后续可以通过永久授权码获取企业access_token，进而调用企业微信相关API为授权企业提供服务。
     */
    @Get("https://open.work.weixin.qq.com/3rdapp/install")
    public Object install(@Body("suite_id") String suite_id, @Body("pre_auth_code") String pre_auth_code, @Body("redirect_uri") String redirect_uri, @Body("state") String state);

    /**
     * https://work.weixin.qq.com/api/doc/90001/90143/90603
     * 获取企业永久授权码
     * 该API用于使用临时授权码换取授权方的永久授权码，并换取授权信息、企业access_token，临时授权码一次有效。
     * <p>
     * 请求方式：POST（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token=SUITE_ACCESS_TOKEN
     *
     * @param suite_access_token 服务商的access_token,最长为512字节
     *                           请求体：
     *                           {
     *                           "auth_code": "auth_code_value" //临时授权码，会在授权成功时附加在redirect_uri中跳转回第三方服务商网站，或通过授权成功通知回调推送给服务商。长度为64至512个字节
     *                           }
     * @return
     */
    @Get("https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code")
    public CorporateBean get_permanent_code(@Body("suite_access_token") String suite_access_token, @JSONBody("auth_code") String auth_code);


    /**
     * https://work.weixin.qq.com/api/doc/90001/90143/90605
     * 获取企业凭证
     * 第三方服务商在取得企业的永久授权码后，通过此接口可以获取到企业的access_token。
     * 获取后可通过通讯录、应用、消息等企业接口来运营这些应用。
     * <p>
     * 此处获得的企业access_token与企业获取access_token拿到的token，本质上是一样的，只不过获取方式不同。获取之后，就跟普通企业一样使用token调用API接口
     * 调用企业接口所需的access_token获取方法如下。
     * <p>
     * 请求方式：POST（HTTPS）
     * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token=SUITE_ACCESS_TOKEN
     *
     * @param suite_access_token 服务商的access_token,最长为512字节
     *                           请求体：
     *                           {
     *                           "auth_corpid": "auth_corpid_value", //授权方corpid
     *                           "permanent_code": "code_value" //永久授权码，通过get_permanent_code获取
     *                           }
     * @return {
     * "errcode":0 ,
     * "errmsg":"ok" ,
     * "access_token": "xxxxxx",
     * "expires_in": 7200
     * }
     */
    @Get("https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code")
    public TokenBean get_corp_token(@Body("suite_access_token") String suite_access_token, @JSONBody("auth_corpid") String auth_corpid, @JSONBody("permanent_code") String permanent_code);


    /**
     * 获取应用的管理员列表
     * https://work.weixin.qq.com/api/doc/90001/90143/90606
     * 第三方服务商可以用此接口获取授权企业中某个第三方应用的管理员列表(不包括外部管理员)，以便服务商在用户进入应用主页之后根据是否管理员身份做权限的区分。
     * <p>
     * 该应用必须与SUITE_ACCESS_TOKEN对应的suiteid对应，否则没权限查看
     * 请求方式：POST（HTTPS）
     * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/get_admin_list?suite_access_token=SUITE_ACCESS_TOKEN
     *
     * @param suite_access_token 服务商的access_token,最长为512字节
     *                           请求体：
     *                           {
     *                           "auth_corpid": "auth_corpid_value", //授权方corpid
     *                           "agentid": 1000046 //授权方安装的应用agentid
     *                           }
     * @return {
     * "errcode": 0,
     * "errmsg": "ok",
     * "admin":[
     * {"userid":"zhangsan","open_userid":"xxxxx","auth_type":1},
     * {"userid":"lisi","open_userid":"yyyyy","auth_type":0}
     * ]
     * }
     */
    @Get("https://qyapi.weixin.qq.com/cgi-bin/service/get_admin_list")
    public Admin_list get_admin_list(@Body("suite_access_token") String suite_access_token, @JSONBody("auth_corpid") String auth_corpid, @JSONBody("agentid") String agentid);
}
