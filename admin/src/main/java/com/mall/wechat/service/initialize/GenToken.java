package com.mall.wechat.service.initialize;


import com.dtflys.forest.annotation.Post;
import com.mall.wechat.bean.TokenBean;
import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.JSONBody;

public interface GenToken {

    /**
     * https://work.weixin.qq.com/api/doc/90001/90142/90593
     * 服务商的token
     * 以corpid、provider_secret（获取方法为：登录服务商管理后台->标准应用服务->通用开发参数，可以看到）换取provider_access_token，
     * 代表的是服务商的身份，而与应用无关。请求单点登录、注册定制化等接口需要用到该凭证。接口详情如下：
     *
     * @param corpid 服务商的corpid
     * @param provider_secret 服务商的secret，在服务商管理后台可见
     * @return
     * {
     *     "errcode":0 ,
     *     "errmsg":"ok" ,
     *     "provider_access_token":"enLSZ5xxxxxxJRL", //服务商的access_token，最长为512字节。
     *     "expires_in":7200 //provider_access_token有效期（秒）
     *  }
     */
    @Post("https://qyapi.weixin.qq.com/cgi-bin/service/get_provider_token")
    public TokenBean get_provider_token(@JSONBody("corpid") String corpid, @JSONBody("provider_secret") String provider_secret);


    /**
     * https://work.weixin.qq.com/api/doc/10975#获取企业access_token
     * 第三方服务商在取得企业的永久授权码后，通过此接口可以获取到企业的access_token。
     * 获取后可通过通讯录、应用、消息等企业接口来运营这些应用。
     *
     * 此处获得的企业access_token与企业获取access_token拿到的token，本质上是一样的，只不过获取方式不同。获取之后，就跟普通企业一样使用token调用API接口
     * 调用企业接口所需的access_token获取方法如下。
     *
     * @param auth_corpid 授权方corpid
     * @param permanent_code 永久授权码，通过get_permanent_code获取
     * @return
     *
     * {
     *     "errcode":0 ,
     *     "errmsg":"ok" ,
     *     "access_token": "xxxxxx", //授权方（企业）access_token,最长为512字节
     *     "expires_in": 7200 //授权方（企业）access_token超时时间
     * }
     */
    @Post("https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token=SUITE_ACCESS_TOKEN")
    public TokenBean get_corp_token(@Body("auth_corpid") String auth_corpid, @Body("permanent_code") String permanent_code);


}

