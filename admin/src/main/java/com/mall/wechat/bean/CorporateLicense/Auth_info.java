/**
  * Copyright 2021 json.cn 
  */
package com.mall.wechat.bean.CorporateLicense;
import java.util.List;

/**
 * Auto-generated: 2021-12-03 17:39:3
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Auth_info {

    //授权信息。如果是通讯录应用，且没开启实体应用，是没有该项的。通讯录应用拥有企业通讯录的全部信息读写权限
    private List<Agent> agent;
    public void setAgent(List<Agent> agent) {
         this.agent = agent;
     }
     public List<Agent> getAgent() {
         return agent;
     }

}