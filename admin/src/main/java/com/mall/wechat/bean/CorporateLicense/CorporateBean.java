/**
  * Copyright 2021 json.cn 
  */
package com.mall.wechat.bean.CorporateLicense;

/**
 * Auto-generated: 2021-12-03 17:39:3
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class CorporateBean {

    private int errcode;
    private String errmsg;
    //授权方（企业）access_token,最长为512字节
    private String access_token;
    //授权方（企业）access_token超时时间（秒）
    private int expires_in;
    //企业微信永久授权码,最长为512字节
    private String permanent_code;
    //代理服务商企业信息。应用被代理后才有该信息
    private Dealer_corp_info dealer_corp_info;
    //授权方企业信息
    private Auth_corp_info auth_corp_info;
    //授权信息。如果是通讯录应用，且没开启实体应用，是没有该项的。通讯录应用拥有企业通讯录的全部信息读写权限
    private Auth_info auth_info;
    //授权管理员的信息，可能不返回（企业互联由上级企业共享第三方应用给下级时，不返回授权的管理员信息）
    private Auth_user_info auth_user_info;
    //推广二维码安装相关信息，扫推广二维码安装时返回。（注：无论企业是否新注册，只要通过扫推广二维码安装，都会返回该字段）
    private Register_code_info register_code_info;
    public void setErrcode(int errcode) {
         this.errcode = errcode;
     }
     public int getErrcode() {
         return errcode;
     }

    public void setErrmsg(String errmsg) {
         this.errmsg = errmsg;
     }
     public String getErrmsg() {
         return errmsg;
     }

    public void setAccess_token(String access_token) {
         this.access_token = access_token;
     }
     public String getAccess_token() {
         return access_token;
     }

    public void setExpires_in(int expires_in) {
         this.expires_in = expires_in;
     }
     public int getExpires_in() {
         return expires_in;
     }

    public void setPermanent_code(String permanent_code) {
         this.permanent_code = permanent_code;
     }
     public String getPermanent_code() {
         return permanent_code;
     }

    public void setDealer_corp_info(Dealer_corp_info dealer_corp_info) {
         this.dealer_corp_info = dealer_corp_info;
     }
     public Dealer_corp_info getDealer_corp_info() {
         return dealer_corp_info;
     }

    public void setAuth_corp_info(Auth_corp_info auth_corp_info) {
         this.auth_corp_info = auth_corp_info;
     }
     public Auth_corp_info getAuth_corp_info() {
         return auth_corp_info;
     }

    public void setAuth_info(Auth_info auth_info) {
         this.auth_info = auth_info;
     }
     public Auth_info getAuth_info() {
         return auth_info;
     }

    public void setAuth_user_info(Auth_user_info auth_user_info) {
         this.auth_user_info = auth_user_info;
     }
     public Auth_user_info getAuth_user_info() {
         return auth_user_info;
     }

    public void setRegister_code_info(Register_code_info register_code_info) {
         this.register_code_info = register_code_info;
     }
     public Register_code_info getRegister_code_info() {
         return register_code_info;
     }

}