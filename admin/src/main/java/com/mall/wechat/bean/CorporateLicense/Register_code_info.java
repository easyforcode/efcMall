/**
 * Copyright 2021 json.cn
 */
package com.mall.wechat.bean.CorporateLicense;

/**
 * Auto-generated: 2021-12-03 17:39:3
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Register_code_info {

    //注册码
    private String register_code;
    //推广包ID
    private String template_id;
    //仅当获取注册码指定该字段时才返回
    private String state;

    public void setRegister_code(String register_code) {
        this.register_code = register_code;
    }

    public String getRegister_code() {
        return register_code;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

}