/**
  * Copyright 2021 json.cn 
  */
package com.mall.wechat.bean.CorporateLicense;

/**
 * Auto-generated: 2021-12-03 17:39:3
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Auth_user_info {

    //授权管理员的userid，可能为空（企业互联由上级企业共享第三方应用给下级时，不返回授权的管理员信息）
    private String userid;
    //授权管理员的open_userid，可能为空（企业互联由上级企业共享第三方应用给下级时，不返回授权的管理员信息）
    private String open_userid;
    //授权管理员的name，可能为空（企业互联由上级企业共享第三方应用给下级时，不返回授权的管理员信息）
    private String name;
    //授权管理员的头像url，可能为空（企业互联由上级企业共享第三方应用给下级时，不返回授权的管理员信息）
    private String avatar;
    public void setUserid(String userid) {
         this.userid = userid;
     }
     public String getUserid() {
         return userid;
     }

    public void setOpen_userid(String open_userid) {
         this.open_userid = open_userid;
     }
     public String getOpen_userid() {
         return open_userid;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setAvatar(String avatar) {
         this.avatar = avatar;
     }
     public String getAvatar() {
         return avatar;
     }

}