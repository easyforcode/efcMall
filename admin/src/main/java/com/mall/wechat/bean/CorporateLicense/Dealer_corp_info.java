/**
  * Copyright 2021 json.cn 
  */
package com.mall.wechat.bean.CorporateLicense;

/**
 * Auto-generated: 2021-12-03 17:39:3
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Dealer_corp_info {

    //代理服务商企业微信id
    private String corpid;
    //代理服务商企业微信名称
    private String corp_name;
    public void setCorpid(String corpid) {
         this.corpid = corpid;
     }
     public String getCorpid() {
         return corpid;
     }

    public void setCorp_name(String corp_name) {
         this.corp_name = corp_name;
     }
     public String getCorp_name() {
         return corp_name;
     }

}