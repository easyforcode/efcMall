/**
  * Copyright 2021 json.cn 
  */
package com.mall.wechat.bean.CorporateLicense;

/**
 * Auto-generated: 2021-12-03 17:39:3
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Agent {

    //授权方应用id
    private int agentid;
    //授权方应用名字
    private String name;
    //授权方应用方形头像
    private String round_logo_url;
    //授权方应用圆形头像
    private String square_logo_url;
    //旧的多应用套件中的对应应用id，新开发者请忽略
    private int appid;
    //授权模式，0为管理员授权；1为成员授权
    private int auth_mode;
    //是否为代开发自建应用
    private boolean is_customized_app;
    //应用对应的权限
    private Privilege privilege;
    //共享了应用的互联企业信息，仅当由企业互联或者上下游共享应用触发的安装时才返回
    private Shared_from shared_from;
    public void setAgentid(int agentid) {
         this.agentid = agentid;
     }
     public int getAgentid() {
         return agentid;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setRound_logo_url(String round_logo_url) {
         this.round_logo_url = round_logo_url;
     }
     public String getRound_logo_url() {
         return round_logo_url;
     }

    public void setSquare_logo_url(String square_logo_url) {
         this.square_logo_url = square_logo_url;
     }
     public String getSquare_logo_url() {
         return square_logo_url;
     }

    public void setAppid(int appid) {
         this.appid = appid;
     }
     public int getAppid() {
         return appid;
     }

    public void setAuth_mode(int auth_mode) {
         this.auth_mode = auth_mode;
     }
     public int getAuth_mode() {
         return auth_mode;
     }

    public void setIs_customized_app(boolean is_customized_app) {
         this.is_customized_app = is_customized_app;
     }
     public boolean getIs_customized_app() {
         return is_customized_app;
     }

    public void setPrivilege(Privilege privilege) {
         this.privilege = privilege;
     }
     public Privilege getPrivilege() {
         return privilege;
     }

    public void setShared_from(Shared_from shared_from) {
         this.shared_from = shared_from;
     }
     public Shared_from getShared_from() {
         return shared_from;
     }

}