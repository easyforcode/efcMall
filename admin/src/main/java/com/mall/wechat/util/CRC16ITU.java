package com.mall.wechat.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CRC16ITU {


    public static int crc16_itu_t(int seed, byte[] src, int len) {
        for (int i = 0; i < len; i++) {
            seed = ((seed >> 8) & 0xFF) | ((seed & 0xFF) << 8);
            seed ^= (src[i] & 0xFF);
            seed ^= (seed & 0xFF) >> 4;
            seed ^= (seed << 12) & 0xFFFF;
            seed ^= ((seed & 0xFF) << 5) & 0xFFFF;
        }
        return seed & 0xFFFF; // Ensure the result is within 16 bits
    }



    public static void main(String[] args) {
        // String filePath = args[0];
        try {
            byte[] data = Files.readAllBytes(Paths.get("/Desktop/lw10_v1.0.19_23120901.ota"));
            int seed = 0x0000; // Initial seed value
            int crc = crc16_itu_t(seed, data, data.length);
            System.out.println(String.format("CRC16-ITU result: 0x%04X", crc));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
