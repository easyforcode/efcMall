package com.mall.config;

public enum ResultEnum {
    /**
     * 请求成功
     */
    SUCCESS(0,""),
    /**
     * 请求失败
     */
    FAIL(-1,"");

    Integer value;
    String msg;

    ResultEnum(Integer value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public Integer getValue() {
        return value;
    }

    public String getMsg() {
        return msg;
    }
}
