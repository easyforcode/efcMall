package com.mall.config.global;

public final class Constants {

    /**
     * 默认分页参数：每页默认20条记录
     */
    public static final int DEFAULT_PAGESIZE = 20;

    /**
     * 数据状态（一般用于保存数据时使用）：有效的
     */
    public static final int STATE_INTEGER_VALID = 1;
    /**
     * 数据状态（一般用于保存数据时使用）：需要审核的
     */
    public static final int STATE_INTEGER_TWO = 2;
    /**
     * 数据状态（一般用于保存数据时使用）：无效的
     */
    public static final int STATE_INTEGER_INVALID = 0;
    /**
     * 数据状态（一般用于保存数据时使用）：已删除
     */
    public static final short STATE_INTEGER_DELETE = 4;

    public static final int JSOUP_TIMEOUT = 5000;

    public static final String SALT_ADMIN = ")^&^xiaoningle.com&*(^";

    public static final String USER_DEFAULT_PASSWORD = "88888888";

    public static final short USER_DEFAULT_LEVEL_TYPE = 0;

    public static final String TOKEN_NAME = "xiaoningle-token";
    public static final String TOKEN_ACCOUT = "account";
    public static final String TOKEN_BUSINESS_CODE = "businessCode";
    public static final String COOKIE_NAME = "xiaoningle-token";
    public static final String SESSION_NAME = "xiaoningle-token";
    public static final String isInterceptorForYes = "Y";
    public static final String isInterceptorForNo = "N";
    public static final String welcome = "welcome";


    /** 正则表达式常量 */
    public static class Regex {
        /** 用户名 */
        public static final String USERNAME = "^\\w{1,50}$";
        /** 手机号码 */
        public static final String TEL = "^1[3-9]\\d{9}$";
        /** 座机 */
        public static final String FIXEDPHONE = "^[0][1-9]{2,3}-[0-9]{7,8}$";
        /** 邮箱 */
        public static final String EMAIL = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        /** 属性名 */
        public static final String PROPERTY_NAME = "^.+$";
    }

}