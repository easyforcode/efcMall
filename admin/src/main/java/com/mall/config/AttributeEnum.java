package com.mall.config;

public enum AttributeEnum {
    /**
     * 拥有模型
     */
    HAVE(1, "拥有模型"),
    /**
     * 没有模型
     */
    NO_HAVE(0, "没有模型");
    Integer value;
    String msg;

    AttributeEnum(Integer value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public Integer getValue() {
        return value;
    }

    public String getMsg() {
        return msg;
    }
}
