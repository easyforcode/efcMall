package com.mall.config;

public enum LoginEnum {
    MOBILE(1, "手机号码登录"),
    WECHAT(2, "微信登录"),
    ALIPAY(3, "支付宝登录");
    Integer value;
    String msg;

    LoginEnum(Integer value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public Integer getValue() {
        return value;
    }

    public String getMsg() {
        return msg;
    }
}
