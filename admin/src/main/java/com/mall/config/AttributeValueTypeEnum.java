package com.mall.config;

public enum AttributeValueTypeEnum {
    /**
     * 内置
     */
    BUILT_IN(1, "内置"),
    /**
     * sku
     */
    SKU(2, "sku"),
    /**
     * spu
     */
    SPU(3, "spu");

    Integer value;
    String msg;

    AttributeValueTypeEnum(Integer value, String msg) {
        this.value = value;
        this.msg = msg;
    }
    public Integer getValue() {
        return value;
    }

    public String getMsg() {
        return msg;
    }
}
