//package com.mall.config;
//
//import com.chok.top.admin.pojo.SystemMenu;
//
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//public class InitWeb {
//    private  Map<String,String> clearInfo;
//    private  Map<String,String> homeInfo;
//    private  Map<String,String> logoInfo;
//    private  Map<String, SystemMenu> menuInfo;
//
//
//    public static InitWeb genJson(List<SystemMenu> list){
//        InitWeb initWeb=new InitWeb();
//        Map<String,String> clearInfo= new LinkedHashMap<String,String>();
//        clearInfo.put("clearUrl","/admin/clear");
//        Map<String,String> homeInfo= new LinkedHashMap<String,String>();
//        homeInfo.put("title","首页");
//        homeInfo.put("icon","fa fa-home");
//        homeInfo.put("href","/admin/index/summary");
//        Map<String,String> logoInfo= new LinkedHashMap<String,String>();
//        logoInfo.put("title","倍好科技");
//        logoInfo.put("image","");
//        logoInfo.put("href","");
//        Map<String,SystemMenu> menuInfo= new LinkedHashMap<String,SystemMenu>();
//        for (SystemMenu systemMenu:list) {
//            menuInfo.put(systemMenu.getCode(),systemMenu);
//        }
//        initWeb.setClearInfo(clearInfo);
//        initWeb.setHomeInfo(homeInfo);
//        initWeb.setLogoInfo(logoInfo);
//        initWeb.setMenuInfo(menuInfo);
//        return initWeb;
//    }
//
//    public static void main(String[] args) {
//        System.out.println(genJson(null));
//    }
//
//    public Map<String, String> getClearInfo() {
//        return clearInfo;
//    }
//
//    public void setClearInfo(Map<String, String> clearInfo) {
//        this.clearInfo = clearInfo;
//    }
//
//    public Map<String, String> getHomeInfo() {
//        return homeInfo;
//    }
//
//    public void setHomeInfo(Map<String, String> homeInfo) {
//        this.homeInfo = homeInfo;
//    }
//
//    public Map<String, String> getLogoInfo() {
//        return logoInfo;
//    }
//
//    public void setLogoInfo(Map<String, String> logoInfo) {
//        this.logoInfo = logoInfo;
//    }
//
//    public Map<String, SystemMenu> getMenuInfo() {
//        return menuInfo;
//    }
//
//    public void setMenuInfo(Map<String, SystemMenu> menuInfo) {
//        this.menuInfo = menuInfo;
//    }
//}
