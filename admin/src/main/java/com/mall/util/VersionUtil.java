package com.mall.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.Properties;

public class VersionUtil {
    private static final Logger logger = LoggerFactory.getLogger(VersionUtil.class);


    public static boolean containParam(String[] array, String param) {
        if (array != null && StringUtils.isNotBlank(param)) {
            String[] var2 = array;
            int var3 = array.length;

            for (int var4 = 0; var4 < var3; ++var4) {
                String str = var2[var4];
                if (param.trim().equals(str)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static void printlnVersion() {
        try {
            Resource resource = new ClassPathResource("git.properties");
            Properties properties = new Properties();
            properties.load(resource.getInputStream());

            for (String key : properties.stringPropertyNames()) {

                System.out.println(String.format("%s = %s", key, properties.getProperty(key)));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
