package com.mall;

import com.mall.util.VersionUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@EnableDiscoveryClient
public class Run {
	private Log logger = LogFactory.getLog(Run.class);

	@Value("${server.port}")
	private int serverPort;

	@Value("${spring.application.name}")
	private String name;

	public static void main(String[] args) throws UnknownHostException {
		if (VersionUtil.containParam(args, "-v")) {
			VersionUtil.printlnVersion();
		}else{
			ConfigurableApplicationContext application = SpringApplication.run(Run.class, args);
		}
//		Environment env = application.getEnvironment();
//		String ip = InetAddress.getLocalHost().getHostAddress();
//		String port = env.getProperty("server.port");
//		String path = env.getProperty("server.servlet.context-path");
//		if (StringUtils.isEmpty(path)) {
//			path = "";
//		}
//		System.out.println("\n\t----------------------------------------------------------\n\t" +
//				"EfcMall is running! Access URLs:\n\t" +
//				"Local访问网址: \t\thttp://localhost:" + port + path + "\n\t" +
//				"External访问网址: \thttp://" + ip + ":" + port + path + "\n\t" +
//				"项目官网: \thttps://www.easyforcode.com\n\t" +
//				"----------------------------------------------------------");
	}

}
