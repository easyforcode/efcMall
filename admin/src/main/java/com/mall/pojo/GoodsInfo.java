package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品索引表（通用属性）
 * <pre>
 *     自动生成代码: 表名 goods_info
 *     id                   int(11)
 *     goods_info_code      varchar(80)
 *     goods_index          varchar(250)
 *     goods_img            varchar(250)
 *     goods_name           varchar(80)
 *     goods_price          varchar(80)
 *     sort                 int(11)
 *     status               tinyint(4)
 *     create_time          datetime
 *     update_time          datetime
 *     remark               varchar(255)
 * </pre>
 *
 * @author caven 2020-07-10 17:47:32
 */
public class GoodsInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private Integer id;

    /**
     * 商品类型
     */
    private String goodsCategoryCode;

    /**
     * 商品CODE 全局唯一
     */
    private String goodsInfoCode;

    /**
     * 商品图片，字段数据来自于模型
     */
    private String goodsIndex;

    /**
     * 商品图片，字段数据来自于模型
     */
    private String goodsImg;

    /**
     * 商品名字，字段数据来自于模型
     */
    private String goodsName;

    /**
     * 商品价格，有可能显示区分价格，字段数据来自于模型
     */
    private String goodsPrice;

    /**
     * 排序 最大排前面
     */
    private Integer sort;

    /**
     * 状态  1：启动  0：禁用
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    public String getGoodsCategoryCode() {
        return goodsCategoryCode;
    }

    public void setGoodsCategoryCode(String goodsCategoryCode) {
        this.goodsCategoryCode = goodsCategoryCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoodsInfoCode() {
        return goodsInfoCode;
    }

    public void setGoodsInfoCode(String goodsInfoCode) {
        this.goodsInfoCode = goodsInfoCode;
    }

    public String getGoodsIndex() {
        return goodsIndex;
    }

    public void setGoodsIndex(String goodsIndex) {
        this.goodsIndex = goodsIndex;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}