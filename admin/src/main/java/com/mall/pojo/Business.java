package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 公司信息表
 * <pre>
 *     自动生成代码: 表名 business
 *     id                    int(11)
 *     business_code         varchar(64)
 *     business_name         varchar(30)
 *     company               varchar(50)
 *     legal_person          varchar(50)
 *     website               varchar(30)
 *     work_phone            varchar(30)
 *     work_mobile           varchar(30)
 *     work_fax              varchar(30)
 *     open_time             datetime
 *     entry_time            datetime
 *     price                 int(11)
 *     level                 int(11)
 *     status                int(11)
 *     last_update_time      datetime
 *     create_time           datetime
 *     remark                varchar(250)
 * </pre>
 *
 * @author caven 2022-01-06 23:12:43
 */
public class Business implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private Integer id;

    /**
     * 商家code
     */
    private String businessCode;

    /**
     * 所属商家名称
     */
    private String businessName;

    /**
     * 公司名字
     */
    private String company;

    /**
     * 公司法人
     */
    private String legalPerson;

    /**
     * 公司网站
     */
    private String website;

    /**
     * 办公电话
     */
    private String workPhone;

    /**
     * 办公电话
     */
    private String workMobile;

    /**
     * 工作传真
     */
    private String workFax;

    /**
     * 开通时间
     */
    private Date openTime;

    /**
     * 过期时间
     */
    private Date entryTime;

    /**
     * 最近一年的缴费价格
     */
    private Integer price;

    /**
     * 管理级别 1为管理员，2为非管理员
     */
    private Integer level;

    /**
     * 状态 ，1为可用 0为封号
     */
    private Integer status;

    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 备注
     */
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getWorkMobile() {
        return workMobile;
    }

    public void setWorkMobile(String workMobile) {
        this.workMobile = workMobile;
    }

    public String getWorkFax() {
        return workFax;
    }

    public void setWorkFax(String workFax) {
        this.workFax = workFax;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}