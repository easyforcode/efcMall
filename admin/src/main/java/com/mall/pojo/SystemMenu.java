package com.mall.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 后端系统菜单表
 * <pre>
 *     自动生成代码: 表名 system_menu
 *     id               int(11)
 *     code             varchar(80)
 *     father_code      varchar(80)
 *     title            varchar(80)
 *     href             varchar(255)
 *     icon             varchar(50)
 *     target           varchar(20)
 *     turn_on          tinyint(4)
 *     sort             int(11)
 *     status           tinyint(4)
 *     create_time      datetime
 *     update_time      datetime
 *     remark           varchar(255)
 * </pre>
 *
 * @author caven 2020-07-04 22:50:50
 */
public class SystemMenu implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean checked = false;

	/**
	 * 自增id
	 */
	private Integer id;

	/**
	 * 菜单唯一编码
	 */
	private String code;

	/**
	 * 上级菜单编码
	 */
	private String fatherCode;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 跳转链接
	 */
	private String href;

	/**
	 * 菜单图标
	 */
	private String icon;

	/**
	 * 跳转模式  1：_self  2：_blank
	 */
	private String target;

	/**
	 * 1：是  0：否  是否默认打开
	 */
	private Integer turnOn;

	/**
	 * 排序 最大排前面
	 */
	private Integer sort;

	/**
	 * 状态  1：启动  2：禁用
	 */
	private Integer status;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 更新时间
	 */
	private Date updateTime;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 级别
	 */
	private String level;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<SystemMenu> child;

	public List<SystemMenu> getChild() {
		return child;
	}

	public void setChild(List<SystemMenu> child) {
		this.child = child;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFatherCode() {
		return fatherCode;
	}

	public void setFatherCode(String fatherCode) {
		this.fatherCode = fatherCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getTurnOn() {
		return turnOn;
	}

	public void setTurnOn(Integer turnOn) {
		this.turnOn = turnOn;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLevel() {
		return level;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}