package com.mall.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 客户信息表
 * <pre>
 *     自动生成代码: 表名 customer_user
 *     id                          int(11)
 *     code_wecaht                 varchar(100)
 *     code_alipay                 varchar(100)
 *     code_finger                 varchar(100)
 *     code_gesture                varchar(100)
 *     code_mobile                 varchar(30)
 *     code_account                varchar(30)
 *     password                    varchar(64)
 *     business_code               varchar(64)
 *     business_name               varchar(30)
 *     personal_code               varchar(50)
 *     employee_bunber             varchar(50)
 *     user_name                   varchar(30)
 *     title                       varchar(30)
 *     gender                      varchar(30)
 *     department_id               int(11)
 *     department_name             varchar(250)
 *     shop                        varchar(200)
 *     work_phone                  varchar(30)
 *     work_fax                    varchar(30)
 *     work_email                  varchar(30)
 *     work_wechat                 varchar(50)
 *     work_qq                     varchar(30)
 *     work_sign                   varchar(250)
 *     is_on_job                   tinyint(4)
 *     entry_time                  datetime
 *     follow_up_customers_no      int(11)
 *     follow_up_suppliers_no      int(11)
 *     level                       int(11)
 *     status                      int(11)
 *     last_login_time             datetime
 *     create_time                 datetime
 *     remark                      varchar(250)
 * </pre>
 *
 * @author caven 2020-07-11 12:53:41
 */
public class CustomerUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private Integer id;

    /**
     * 微信识别帐号
     */
    private String codeWecaht;

    /**
     * 支付宝识别帐号
     */
    private String codeAlipay;

    /**
     * 指纹识别帐号 - 手机端使用
     */
    private String codeFinger;

    /**
     * 手势识别帐号 - 手机端使用
     */
    private String codeGesture;

    /**
     * 电话
     */
    private String codeMobile;

    /**
     * 登录帐号
     */
    private String codeAccount;

    /**
     * 密码 code_account使用
     */
    private String password;

    /**
     * 商家code
     */
    private String businessCode;

    /**
     * 所属商家名称
     */
    private String businessName;

    /**
     * 个人编码
     */
    private String personalCode;

    /**
     * 员工编码
     */
    private String employeeBunber;

    /**
     * 用户名，用于显示
     */
    private String userName;

    /**
     * 职称
     */
    private String title;

    /**
     * 性别
     */
    private String gender;

    /**
     * 部门
     */
    private Integer departmentId;

    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 所属门店
     */
    private String shop;

    /**
     * 办公电话
     */
    private String workPhone;

    /**
     * 工作传真
     */
    private String workFax;

    /**
     * 工作邮箱
     */
    private String workEmail;

    /**
     * 工作微信
     */
    private String workWechat;

    /**
     * 工作QQ
     */
    private String workQq;

    /**
     * 签名
     */
    private String workSign;

    /**
     * 是否在职 1：是  0：否（0必须是封号）
     */
    private Integer isOnJob;

    /**
     * 入职时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date entryTime;

    /**
     * 跟进客户数量
     */
    private Integer followUpCustomersNo;

    /**
     * 跟进供应商数量
     */
    private Integer followUpSuppliersNo;

    /**
     * 管理级别 1为管理员，2为非管理员
     */
    private Integer level;

    /**
     * 状态 ，1为可用 0为封号
     */
    private Integer status;

    /**
     * 最后登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastLoginTime;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 备注
     */
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeWecaht() {
        return codeWecaht;
    }

    public void setCodeWecaht(String codeWecaht) {
        this.codeWecaht = codeWecaht;
    }

    public String getCodeAlipay() {
        return codeAlipay;
    }

    public void setCodeAlipay(String codeAlipay) {
        this.codeAlipay = codeAlipay;
    }

    public String getCodeFinger() {
        return codeFinger;
    }

    public void setCodeFinger(String codeFinger) {
        this.codeFinger = codeFinger;
    }

    public String getCodeGesture() {
        return codeGesture;
    }

    public void setCodeGesture(String codeGesture) {
        this.codeGesture = codeGesture;
    }

    public String getCodeMobile() {
        return codeMobile;
    }

    public void setCodeMobile(String codeMobile) {
        this.codeMobile = codeMobile;
    }

    public String getCodeAccount() {
        return codeAccount;
    }

    public void setCodeAccount(String codeAccount) {
        this.codeAccount = codeAccount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public String getEmployeeBunber() {
        return employeeBunber;
    }

    public void setEmployeeBunber(String employeeBunber) {
        this.employeeBunber = employeeBunber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getWorkFax() {
        return workFax;
    }

    public void setWorkFax(String workFax) {
        this.workFax = workFax;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public void setWorkEmail(String workEmail) {
        this.workEmail = workEmail;
    }

    public String getWorkWechat() {
        return workWechat;
    }

    public void setWorkWechat(String workWechat) {
        this.workWechat = workWechat;
    }

    public String getWorkQq() {
        return workQq;
    }

    public void setWorkQq(String workQq) {
        this.workQq = workQq;
    }

    public String getWorkSign() {
        return workSign;
    }

    public void setWorkSign(String workSign) {
        this.workSign = workSign;
    }

    public Integer getIsOnJob() {
        return isOnJob;
    }

    public void setIsOnJob(Integer isOnJob) {
        this.isOnJob = isOnJob;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Integer getFollowUpCustomersNo() {
        return followUpCustomersNo;
    }

    public void setFollowUpCustomersNo(Integer followUpCustomersNo) {
        this.followUpCustomersNo = followUpCustomersNo;
    }

    public Integer getFollowUpSuppliersNo() {
        return followUpSuppliersNo;
    }

    public void setFollowUpSuppliersNo(Integer followUpSuppliersNo) {
        this.followUpSuppliersNo = followUpSuppliersNo;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}