package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录日志表
 * <pre>
 *     自动生成代码: 表名 customer_user_login_log
 *     id                 int(11)
 *     business_code      varchar(64)
 *     login_type         int(11)
 *     code_account       varchar(50)
 *     user_name          varchar(50)
 *     login_ip           varchar(50)
 *     login_mac          varchar(60)
 *     login_imei         varchar(60)
 *     login_token        varchar(60)
 *     login_time         datetime
 *     remark             varchar(250)
 * </pre>
 *
 * @author caven 2020-07-11 14:36:30
 */
public class CustomerUserLoginLog implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 
	 * 自增id
	 */
	private Integer id;

	/** 
	 * 商家code
	 */
	private String businessCode;

	/** 
	 * 1:账号登录  2：微信  3：支付宝  4：电话  5：指纹  6：图形
	 */
	private Integer loginType;

	/** 
	 * 登录帐号
	 */
	private String codeAccount;

	/** 
	 * 用户名，用于显示
	 */
	private String userName;

	/** 
	 * 登录IP
	 */
	private String loginIp;

	/** 
	 * 登录Mac地址 针对手机
	 */
	private String loginMac;

	/** 
	 * 登录imei 针对手机
	 */
	private String loginImei;

	/** 
	 * 登录token
	 */
	private String loginToken;

	/** 
	 * 最后登录时间
	 */
	private Date loginTime;

	/** 
	 * 备注
	 */
	private String remark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBusinessCode() {
		return businessCode;
	}

	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}

	public Integer getLoginType() {
		return loginType;
	}

	public void setLoginType(Integer loginType) {
		this.loginType = loginType;
	}

	public String getCodeAccount() {
		return codeAccount;
	}

	public void setCodeAccount(String codeAccount) {
		this.codeAccount = codeAccount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getLoginMac() {
		return loginMac;
	}

	public void setLoginMac(String loginMac) {
		this.loginMac = loginMac;
	}

	public String getLoginImei() {
		return loginImei;
	}

	public void setLoginImei(String loginImei) {
		this.loginImei = loginImei;
	}

	public String getLoginToken() {
		return loginToken;
	}

	public void setLoginToken(String loginToken) {
		this.loginToken = loginToken;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}