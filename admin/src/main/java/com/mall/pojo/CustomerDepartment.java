package com.mall.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 企业部门
 * <pre>
 *     自动生成代码: 表名 customer_department
 *     id                 int(11)
 *     business_code      varchar(64)
 *     show_code          varchar(80)
 *     code               varchar(80)
 *     father_code        varchar(80)
 *     title              varchar(80)
 *     href               varchar(255)
 *     icon               varchar(50)
 *     target             varchar(20)
 *     turn_on            tinyint(4)
 *     sort               int(11)
 *     status             tinyint(4)
 *     create_time        datetime
 *     update_time        datetime
 *     remark             varchar(255)
 * </pre>
 *
 * @author caven 2020-07-15 16:54:57
 */
public class CustomerDepartment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private Integer id;

    /**
     * 商家code
     */
    private String businessCode;

    /**
     * 用户自定义的code，用于展示
     */
    private String showCode;

    /**
     * 菜单唯一编码
     */
    private String code;

    /**
     * 上级菜单编码
     */
    private String fatherCode;

    /**
     * 标题
     */
    private String title;

    /**
     * 跳转链接
     */
    private String href;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 跳转模式  1：_self  2：_blank
     */
    private String target;

    /**
     * 1：是  0：否  是否默认打开
     */
    private Integer turnOn;

    /**
     * 排序 最大排前面
     */
    private Integer sort;

    /**
     * 状态  1：启动  2：禁用
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    private boolean open = true;

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<CustomerDepartment> child;

    public List<CustomerDepartment> getChild() {
        return child;
    }

    public void setChild(List<CustomerDepartment> child) {
        this.child = child;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getShowCode() {
        return showCode;
    }

    public void setShowCode(String showCode) {
        this.showCode = showCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFatherCode() {
        return fatherCode;
    }

    public void setFatherCode(String fatherCode) {
        this.fatherCode = fatherCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getTurnOn() {
        return turnOn;
    }

    public void setTurnOn(Integer turnOn) {
        this.turnOn = turnOn;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}