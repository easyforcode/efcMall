package com.mall.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 后台用户表
 * <pre>
 *     自动生成代码: 表名 system_user
 *     id                   int(11)
 *     wx_code              varchar(200)
 *     staff_code           varchar(30)
 *     tel                  varchar(30)
 *     password             varchar(64)
 *     user_name            varchar(30)
 *     qq                   varchar(11)
 *     email                varchar(30)
 *     position             varchar(20)
 *     level                int(11)
 *     status               int(11)
 *     last_login_time      datetime
 *     create_time          datetime
 *     remark               varchar(250)
 * </pre>
 *
 * @author caven 2020-07-05 17:04:14
 */
public class SystemUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private Integer id;

    /**
     * 微信识别帐号
     */
    private String wxCode;

    /**
     * 登录帐号
     */
    private String staffCode;

    /**
     * 电话
     */
    private String tel;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户名，用于显示
     */
    private String userName;

    /**
     * 腾讯QQ号
     */
    private String qq;

    /**
     * 邮箱号码
     */
    private String email;

    /**
     * 职位
     */
    private String position;

    /**
     * 级别 1为管理员，2为非管理员
     */
    private Integer level;

    /**
     * 状态 ，1为可用 0为封号
     */
    private Integer status;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 备注
     */
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWxCode() {
        return wxCode;
    }

    public void setWxCode(String wxCode) {
        this.wxCode = wxCode;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}