//package com.mall.spring;
//
//import org.springframework.beans.BeansException;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ApplicationContextUtil implements ApplicationContextAware {
//
//    private static ApplicationContext applicationContext;
//
//    @Override
//    public void setApplicationContext(ApplicationContext context) throws BeansException {
//        applicationContext = context;
//    }
//
//
//    public static ApplicationContext getApplicationContext() {
//        if (applicationContext == null) {
//            throw new IllegalStateException("applicationContext is null,SpringContextHolder未成功初始化");
//        }
//        return applicationContext;
//    }
//
//    /**
//     *  根据Bean的名称从Spring容器中获取Bean对象
//     *
//     *  @param name
//     *  @return
//     */
//    public static Object getBean(String name) {
//        return getApplicationContext().getBean(name);
//    }
//
//
//    /**
//     * 根据Bean名称、Class从Spring容器中获取Bean对象
//     *
//     * @param clazz
//     * @return T 返回类型
//     */
//    public static <T> T getBean(Class<T> clazz) {
//        return getApplicationContext().getBean(clazz);
//
//    }
//
//    /**
//     * 根据Bean名称、Class从Spring容器中获取Bean对象
//     *
//     * @param name
//     * @param clazz
//     * @return T 返回类型
//     */
//    public static <T> T getBean(String name, Class<T> clazz) {
//        try {
//            return getApplicationContext().getBean(name, clazz);
//        } catch (Exception e) {
//            return getBean(clazz);
//        }
//    }
//
//}