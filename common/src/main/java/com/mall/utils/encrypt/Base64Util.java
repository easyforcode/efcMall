package com.mall.utils.encrypt;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class Base64Util {
    final static Base64.Decoder decoder = Base64.getDecoder();
    final static Base64.Encoder encoder = Base64.getEncoder();

    public static void main(String[] args) throws UnsupportedEncodingException {
        String text = "hello";
        byte[] textByte = text.getBytes("UTF-8");

        for (int i = 0; i < textByte.length; i++) {
            System.out.println(textByte[i]);
        }

        String encodedText = encoder.encodeToString(textByte);
        System.out.println(encodedText);

        byte[] decoderByte=decoder.decode(encodedText);

        System.out.println(new String(decoderByte));

    }
}
