package com.mall.utils.encrypt;

import com.google.common.collect.ImmutableSortedMap;

public class ImmutableSortedMapExample {

    public static void main(String[] args) {
        // 创建一个按键排序的不可变映射
        ImmutableSortedMap<String, Integer> immutableMap = ImmutableSortedMap.<String, Integer>naturalOrder()
                .put("apple", 2)
                .put("banana", 3)
                .put("cherry", 1)
                .build();

        // 输出映射的内容，按键排序
        for (String key : immutableMap.keySet()) {
            System.out.println(key + ": " + immutableMap.get(key));
        }
    }
}
