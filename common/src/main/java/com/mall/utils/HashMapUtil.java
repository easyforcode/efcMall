package com.mall.utils;

/**
 * 处理HashMap的工具类
 * 
 * @author caven 2014/06/04
 */
public class HashMapUtil {

	/**
	 * 获取hashmap的初始容量
	 * 
	 * 当HashMap的数据大小>=容量*加载因子时，HashMap会将容量扩容<br/>
	 * 扩容带来一系列的运算，新建一个是原来容量2倍的数组，对原有元素全部重新哈希，<br/>
	 * 在知道hashmap需要储存的容量时，可以通过此方法获取hashmap的初始容量,用来减少不必要的运算<br/>
	 * 
	 * @param arraySize
	 *            需要保存的数据长度
	 * @return
	 */
	public static int getSize(int arraySize) {
		int size = 0;
		// 刚好达到hashmap扩容的边界
		size = arraySize + arraySize / 3;
		// 添加多6个大小，避免扩容,同时也避免arraySize为0的情况
		size += 6;
		return size;
	}

	public static void main(String[] args) {
		
		System.out.println(getSize(62));
	}
}
