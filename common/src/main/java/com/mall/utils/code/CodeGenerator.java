//package com.mall.utils.code;
//
////import org.apache.commons.lang3.StringUtils;
//
//import java.util.*;
//
///**
// * 生成以入库时间为基准的激活码
// *
// * @author caven 2023/7/10
// */
//public class CodeGenerator {
//	private static int asctivationCounter = 0;
//
//	/**
//	 * 取得2014年3月3日至今的分钟数，并转换成36进制的字符串返回<br/>
//	 * 大概2015年前是生成3位， 2015年到2040年间是4位，随着年份增加不停的递增<br/>
//	 * 例如返回36进制的字符串为 My6，<br/>
//	 * 如果length==0，则返回My6 <br/>
//	 * 如果length==5，则返回00My6
//	 *
//	 * @param length
//	 *            指定返回字符串的最小长度
//	 * @return
//	 */
//	private static String currentTimeToString(int length) {
//		// 取得2014年3月3日至今的毫秒数（没必要从1970/1/1开始算起 减少时间差）
//		long a = System.currentTimeMillis() - 1393776000000L;
//		// 取得分钟数
//		int time = (int) (a / (1000 * 60));
//		return ThirtySixCountUtil.gen(time, length);
//	}
//
//	/**
//	 * 将进程id转换成相应的36进制字符串 如果生成的字符串多于2为，这只会返回前2位
//	 *
//	 * @return
//	 */
//	private static String getBasePid() {
//		int num = 0;
//		try {
//			num = Integer.parseInt(OSUtil.getPid());
//		} catch (NumberFormatException e) {
//			Random rand = new Random();
//			num = rand.nextInt(65536);
//		}
//		String value = ThirtySixCountUtil.gen(num, 2);
//		if (value.length() > 2) {
//			return StringUtils.substring(value, 0, 2);
//		}
//		return value;
//	}
//
//	/**
//	 * 将IP转换成相应的36进制字符串 如果生成的字符串多于2为，这只会返回前2位
//	 *
//	 * @return
//	 */
//	private static String getBaseIP() {
//		int num = 0;
//		try {
//			num = Integer.parseInt(OSUtil.getIpLastNumber());
//		} catch (NumberFormatException e) {
//			Random rand = new Random();
//			num = rand.nextInt(65536);
//		}
//		String value = ThirtySixCountUtil.gen(num, 2);
//		if (value.length() > 2) {
//			return StringUtils.substring(value, 0, 2);
//		}
//		return value;
//	}
//
//	/**
//	 * 获取指定长度的随机数组
//	 *
//	 * @param length
//	 *            随机字符串的长度
//	 * @param count
//	 *            数组的数量
//	 * @return
//	 */
//	public static List<String> genRandomList(int length, int count) {
//		Random r = new Random();
//		Map<String, String> map = new HashMap<String, String>(
//				HashMapUtil.getSize(count));
//		String randomString = null;
//		for (int i = 0; i < count; i++) {
//			randomString = StringUtil.getRandomString(r, length);
//			boolean check = true;
//			while (check) {
//				try {
//					if (map.containsKey(randomString)) {
//						randomString = StringUtil.getRandomString(r, length);
//					} else {
//						check = false;
//						map.put(randomString, "");
//					}
//				} catch (Exception e) {
//					check = false;
//				}
//			}
//		}
//		List<String> list = new ArrayList<String>(count);
//		for (Map.Entry<String, String> entry : map.entrySet()) {
//			list.add(entry.getKey());
//		}
//		return list;
//	}
//
//	public static final synchronized String genCode() {
//		List<String> list = newActivationCodeList(null, 15, 1);
//		return list.get(0);
//	}
//
//	/**
//	 * 产生指定数量和长度的激活码<br/>
//	 * 同一个jvm，一分钟内只能最多同时生成64次激活码（一次生成的激活码数量不限），超过有几率相同<br/>
//	 * 对于激活码长度减去前缀的长度小于或等于12的激活码，程序只会用时间和次数去验证，如果不同的jvm同时运行，有几率出现一致的激活码<br/>
//	 * 对于激活码长度减去前缀的长度大于12的激活码，程序保证其唯一性<br/>
//	 * length-prefix的长度必须大于10<br/>
//	 *
//	 * @param prefix
//	 *            激活码的前缀 该参数可以为空
//	 * @param length
//	 *            激活码的长度，包含前缀 长度必须大于10
//	 * @param count
//	 *            产生激活码的数量
//	 * @return
//	 */
//	public static final synchronized List<String> newActivationCodeList(
//			String prefix, int length, int count) {
//		if (count < 1) {
//			throw new IllegalArgumentException("参数有误，激活码数量必须大于0");
//		}
//		if (asctivationCounter == 35) {
//			asctivationCounter = 0;
//		}
//		if (prefix == null) {
//			prefix = "";
//		}
//		// 默认全部为大写字母
//		prefix = StringUtils.upperCase(prefix);
//		if (length - prefix.length() < 10) {
//			throw new IllegalArgumentException("参数有误，激活码长度 减去前缀的长度 必须不小于10");
//		}
//
//		if (length - prefix.length() <= 12) {
//			// 生成最短为4位的唯一时间
//			String baseDate = currentTimeToString(4);
//			// 只会生成一位
//			String baseTimes = ThirtySixCountUtil.gen(++asctivationCounter);
//			List<String> list = genRandomList(length - prefix.length()
//					- baseDate.length() - 1, count);
//			return genResultList(prefix, baseDate + baseTimes, list, length);
//		} else {
//			// 生成最短为4位的唯一时间
//			String baseDate = currentTimeToString(4);
//			// 只会生成一位
//			String baseTimes = ThirtySixCountUtil.gen(++asctivationCounter);
//			// 生成2位
//			String basePid = getBasePid();
//			// 生成2位
//			String BaseIP = getBaseIP();
//
//			List<String> list = genRandomList(length - prefix.length()
//					- baseDate.length() - 5, count);
//			return genResultList(prefix, baseDate + baseTimes + basePid
//					+ BaseIP, list, length);
//		}
//	}
//
//	private static List<String> genResultList(String prefix, String base,
//			List<String> list, int length) {
//		List<String> resultList = new ArrayList<String>(list.size());
//		for (String value : list) {
//			StringBuilder buf = new StringBuilder(length);
//			buf.append(prefix);
//			buf.append(value.substring(0, 1));
//			buf.append(base.substring(0, 1));
//			buf.append(base.substring(1, 2));
//			buf.append(value.substring(1, 2));
//			buf.append(base.substring(2, 3));
//			buf.append(value.substring(2, 3));
//			buf.append(base.substring(3, 4));
//			buf.append(value.substring(3, 4));
//			buf.append(base.substring(4, base.length()));
//			buf.append(value.substring(4, value.length()));
//			resultList.add(buf.toString());
//		}
//		return resultList;
//	}
//
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) throws Exception {
//		System.out.println(genCode());
////		long t1 = System.currentTimeMillis();
////		List<String> list = newActivationCodeList("yl", 12, 300);
////		for (String value : list) {
////			System.out.println(value);
////		}
////		long t2 = System.currentTimeMillis();
////		System.out.println("time=" + (t2 - t1) + " ms");
//	}
//
//}
