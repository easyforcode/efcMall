package com.mall.utils.code;

import java.util.Random;

/**
 * 处理String相关的工具类
 *
 * @author caven 2023/10/10
 */
public class StringUtil {
    /**
     * 常量定义
     */
    private static final String BUFFER = new String(
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");

    /**
     * 获取指定长度的随机字符串
     *
     * @param length 随机字符串的长度
     * @return
     */
    public static String getRandomString(int length) { // length 字符串长度
        return getRandomString(new Random(), length);
    }

    /**
     * 获取指定长度的随机字符串
     *
     * @param r      随机数对象（java.util.Random）
     * @param length 随机字符串的长度
     * @return
     */
    public static String getRandomString(Random r, int length) { // length 字符串长度
        StringBuffer sb = new StringBuffer();
        int range = BUFFER.length();
        for (int i = 0; i < length; i++) {
            sb.append(BUFFER.charAt(r.nextInt(range)));
        }
        return sb.toString();
    }

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        System.out.println(getRandomString(10));
    }

}