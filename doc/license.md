# 许可证：Apache License 2.0
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg)](LICENSE)  

## 1. 定义

“本软件”指您在这份许可下所发布的项目及其相关文档。

“您”是指使用本软件的个人或法律实体。

## 2. 授予许可

在遵循以下条款和条件的情况下，获得了免费、非独家的全球许可，允许您使用、复制、修改和分发本软件。

## 3. 条款

您必须在所有的副本和派发版本中保留以下许可声明，包括本列表中的所有文件：

- 版权声明；
- 本许可声明；
- 免责声明。

## 4. 免责声明

本软件是按“原样”提供的，没有任何明示或暗示的保证。作者不对任何使用本软件的后果承担任何责任。

## 5. 版权信息

本软件受版权法保护。版权所有 (c) [2019年8月8日] [易为创码]。

## 6. 链接

当您在软件中包含代码的副本或衍生作品时，无论是作为源代码还是二进制文件，都应当保留原始许可文件，并在适当的地方提供指向本许可文件的链接。

## 7. 适用法律

本许可受适用法律的约束，如有冲突，以适用法律为准。

## 8. 联系方式

如果您有任何问题或疑虑，请联系我们：[18566298879]。
