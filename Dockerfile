# 使用 Maven 的 official OpenJDK 8 基础镜像
FROM maven:3.8.4-openjdk-8-slim AS build

# 设置工作目录
WORKDIR /app

# 复制 Maven 依赖文件
COPY pom.xml .

# 下载依赖并构建应用
RUN mvn dependency:go-offline

# 复制整个项目
COPY . .

# 打包应用
RUN mvn package

# 使用官方的 OpenJDK 8 基础镜像
FROM openjdk:8-jdk-alpine

# 设置工作目录
WORKDIR /app

# 从 build 阶段复制 JAR 文件到当前镜像
COPY --from=build /app/target/mall.jar /app/mall.jar

# 定义容器启动时执行的命令
CMD ["java", "-jar", "mall.jar"]
